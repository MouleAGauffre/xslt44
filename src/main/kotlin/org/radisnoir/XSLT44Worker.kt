package org.radisnoir

import com.github.benmanes.caffeine.cache.Cache
import com.github.benmanes.caffeine.cache.Caffeine
import io.quarkus.scheduler.Scheduled
import java.io.ByteArrayOutputStream
import java.io.StringReader
import java.security.MessageDigest
import java.util.*
import java.util.concurrent.TimeUnit
import javax.ws.rs.Consumes
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import javax.xml.transform.Transformer
import javax.xml.transform.TransformerConfigurationException
import javax.xml.transform.TransformerException
import javax.xml.transform.TransformerFactory
import javax.xml.transform.stream.StreamResult
import javax.xml.transform.stream.StreamSource
import org.apache.log4j.LogManager
import org.apache.log4j.Logger
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput
import redis.clients.jedis.Jedis

/**
 * @author Guillaume
 * XSL transformation service
 */
@Path("/")
class XSLT44Worker {

    private val cache: Cache<String, CachedTransformer> = Caffeine.newBuilder()
        .expireAfterWrite(1, TimeUnit.HOURS)
        .maximumSize(System.getenv("MAX_CACHE_SIZE").toLong()).build()
    private val keySetToCommit = HashSet<String>()

    init {
        // If the current worker is launched after the deployment it can synchronize it's cache with the shared redis
        // cache
        syncCache()
    }

    /**
     * @param req Multiform data with two strings: doc for with xml document and stylesheet with stylesheet content
     * @return The xml result of the transformation
     */
    @POST
    @Path("/transform")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_XML)
    fun transform(req: MultipartFormDataInput): ByteArray? {
        val stylesheetContent = req.formDataMap["stylesheet"]?.first()?.bodyAsString!!

        val md = MessageDigest.getInstance("MD5")
        md.update(stylesheetContent.toByteArray())
        val digest = String(md.digest())

        var cachedTransformer: CachedTransformer? = cache.getIfPresent(digest)
        // If the stylesheet isn't cached
        if (cachedTransformer == null) {
            try {
                cachedTransformer = CachedTransformer(
                    stylesheetContent,
                    tFactory.newTransformer(StreamSource(StringReader(stylesheetContent)))
                )

                cache.put(digest, cachedTransformer)
                // the cached stylesheet will be committed to the redis cache on the next cache sync so the other
                // instances share this local cache
                keySetToCommit.add(digest)

            } catch (e: TransformerConfigurationException) {
                logger.error("A transformer can't be instantiated for stylesheet:\n $stylesheetContent", e)
                return ByteArray(0)
            }
        }

        return try {
            val outStream = ByteArrayOutputStream()
            cachedTransformer.transformer.transform(
                StreamSource(StringReader(req.formDataMap["doc"]?.first()?.bodyAsString!!)),
                StreamResult(outStream)
            )
            outStream.toByteArray()

        } catch (e: TransformerException) {
            logger.error("The xsl transformation failed", e)
            ByteArray(0)
        }
    }

    /**
     * Synchronize every minute the local caffeine cache and the redis cache which is shared with other instances.
     * The recently locally cached stylesheets will be sent to the redis shared cache and the cached elements in the
     * redis cache which aren't cached locally will be cached in the caffeine cache
     */
    @Scheduled(every = "1m")
    private fun syncCache() {
        keySetToCommit.forEach {
            jedisClient.hset("cache", it, cache.getIfPresent(it)!!.stylesheetContent)
        }
        keySetToCommit.clear()

        for (pair in jedisClient.hkeys("stylesheet-cache").zip(jedisClient.hvals("stylesheet-cache"))) {
            if (cache.getIfPresent(pair.first) == null) {
                try {
                    cache.put(
                        pair.first,
                        CachedTransformer(pair.second, tFactory.newTransformer(StreamSource(StringReader(pair.second))))
                    )

                } catch (e: TransformerConfigurationException) {
                    logger.error("A transformer can't be instantiated for stylesheet:\n $pair.second", e)
                }
            }
        }
    }

    private data class CachedTransformer(val stylesheetContent: String, val transformer: Transformer)

    companion object {
        private var logger: Logger = LogManager.getLogger(XSLT44Worker::class.simpleName)
        private val tFactory: TransformerFactory = TransformerFactory.newInstance()
        private val jedisClient: Jedis = Jedis(System.getenv("REDIS_HOST"))
    }

}
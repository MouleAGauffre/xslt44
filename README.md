# xslt44 project

**_xslt44_** is a xsl transformation cluster. It uses:

- Quarkus, the Supersonic Subatomic Java Framework
- Caffeine for worker level caching
- Redis for cache synchronization between workers with Jedis client
- Quartz for cache synchronization scheduling
- Smallrye health for health checks

We use those 2 docker images:

- pressepuree/redis-lfu: it's a standard redis + some configuration to make it a lfu cache
- pressepuree/xslt44-worker-jvm: the xslt worker node

## Running the application in dev mode

You need kubectl installed.

Configuration:
```shell script
kubectl apply -f .\src\main\k8s\config.yml
```

Deploying the cluster
```shell script
kubectl apply -f .\src\main\k8s\xslt44-jvm.yml
```

It will start:

- a redis pod
- 2 worker pods
- a load balancer
- an autoscaler for the worker nodes

If you want to customize work on xslt44-jvm.yml or config.yml

## Send a test request

You need to POST to localhost:8080/transform with multipart form data. It contains:

- doc: the xml document content
- stylesheet: the xsl document content

## generate yourself a client

Using the OpenAPI spec here: https://app.swaggerhub.com/apis/RadisCorp/xslt44/1.0.0#free or in ./src/main/resources/api.yml

## Clean up

```shell script
kubectl delete -f .\src\main\k8s\xslt44-jvm.yml
kubectl delete -f .\src\main\k8s\config.yml
```